<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Abonne;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/user", name="useraccount")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        $data = array(
            'title'     => "Mon Compte",
            'user'      => $this->getUser()
        );
        return $this->render(':user:profil.html.twig', $data);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request){
        $authenticationUtils = $this->get('security.authentication_utils');

        //if error
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username
        $lastUsername = $authenticationUtils->getLastUsername();

        $data = array(
            'title'         => 'Connexion',
            'last_username' => $lastUsername,
            'error'         => $error
        );

        return $this->render('user/login.html.twig', $data);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Response
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/inscription", name="registerpage")
     */
    public function registrationAction(Request $request){
        $user = new Abonne();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $password = $user->getPlainPassword();
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        $data = array(
            'title'     => 'Inscription',
            'form'      => $form->createView()
        );
        return $this->render('form/register.html.twig', $data);
    }

}
