<?php


namespace AppBundle\Controller;

use AppBundle\Entity\AlbumRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DiscothequeController extends Controller
{
    /**
     * @Route("/discotheque", name="discothequepage")
     */
    public function indexAction()
    {
        return $this->render('discotheque/accueil.html.twig');
    }

    /**
     * @param $idAlbum
     *
     * @Route("/discotheque/album/{idAlbum}", name="oneAlbum")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAlbumAction($idAlbum)
    {
        $album = $this->getDoctrine()
            ->getRepository('AppBundle:Album')
            ->find($idAlbum);
        if (!$album){
            throw $this->createNotFoundException(
                'Aucun album ne correspond à votre demande'
            );
        }
        $data = array(
            'title'     => 'Album',
            'album'     => $album
        );
        return $this->render('discotheque/ficheAlbum.html.twig', $data);
    }

    /**
     * @Route("/discotheque/albums/{index}", name="listAlbums")
     * @param $index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAlbumListAction($index){
        $em = $this->getDoctrine()->getManager();

        $albums = $em->getRepository('AppBundle:Album')
            ->findAllByPageAndSort($index);

        $pagination = array(
            'index'     => $index,
            'nbPages'   => ceil(count($albums) / AlbumRepository::NB_MAX_PER_PAGE),
            'route'     => 'listAlbums',
            'paramsRoute'=> array()
        );
        $data = array(
            'title'     => 'Albums',
            'pagination'=> $pagination,
            'listAlbums'    => $albums
        );
        return $this->render('discotheque/listAlbums.html.twig', $data);
    }

    /**
     * Affiche la liste des albums par genre
     *
     * @param $codeGenre
     * @param int $index
     * @Route("/discotheque/instrument/{codeGenre}/{index}", name='albumByGenre')
     */
    public function getGenreAction($codeGenre, $index = 1){
        $dr = $this->getDoctrine();
        $albums = $dr->getDoctrine()
            ->getRepository('Album')
            ->findAllByGenre($codeGenre, $index);

        $genres = $dr->getRepository('AppBundle:Genre')
            ->findAll();

        if(!$genres || !$albums){
            throw $this->createNotFoundException(
                "Le genre demander n'existe pas."
            );
        }

        $pagination = array(
            'index'     => $index,
            'nbPages'   => ceil(count($albums) / AlbumRepository::NB_MAX_PER_PAGE),
            'route'     => 'albumsByGenre',
            'paramsRoute'=> array('codeGenre'=> $codeGenre)
        );

        $data = array(
            'title'     => 'Album / Genre',
            'albums'    => $albums,
            'genre'     => $genres
        );


    }

    /**
     * Génére une image à partir de la base de données et la retourne au client
     * @see http://info-timide.iut.u-bordeaux.fr/Cours/Symfony/SymfImage/
     *
     * @param $id identifiant de l'entité de la photo demandée
     * @param $type le type de l'entité de la photo demandée
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/photo/{id}/{type}", name="photo")
     * @Template()
     */
    public function photoAction($id, $type){
        $image = $this->getDoctrine()
            ->getRepository('AppBundle:'.$type)
            ->find($id);
        $image = stream_get_contents($image->getImage());
        $image = pack("H*", $image);
        $response = new Response();
        $response->headers->set('Content-type', 'image/jpeg');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->setContent($image);
        return $response;
    }
}
