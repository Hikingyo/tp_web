<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('nomAbonne', TextType::class)
            ->add('prenomAbonne', TextType::class)
            ->add('adresse', TextareaType::class)
            ->add('plainPassword', RepeatedType::class, array(
                'type'          => PasswordType::class,
                'first_options' => array('label' => 'Mot de passe'),
                'second_options'    => array('label' => 'Vérification Mot de passe')
                    )
                )
            ->add('Enregistrer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Abonne',
        ));
    }

    public function getName()
    {
        return 'app_bundle_user_type';
    }
}
