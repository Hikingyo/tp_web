<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 *
 * @ORM\Table(name="Album", indexes={@ORM\Index(name="IDX_F8594147E1990660", columns={"Code_Genre"}), @ORM\Index(name="IDX_F8594147EA8CE117", columns={"Code_Editeur"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AlbumRepository")
 */
class Album
{
    /**
     * Album constructor.
     */
    public function __construct()
    {
        $this->codeGenre = new ArrayCollection();
        $this->codeEditeur = new Editeur();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="Code_Album", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_Album", type="string", length=400, nullable=false)
     */
    private $titreAlbum;

    /**
     * @var integer
     *
     * @ORM\Column(name="Année_Album", type="integer", nullable=true)
     */
    private $anneeAlbum;

    /**
     * @var binary
     *
     * @ORM\Column(name="Pochette", type="binary", nullable=true)
     */
    private $pochette;

    /**
     * @var string
     *
     * @ORM\Column(name="ASIN", type="string", length=20, nullable=true)
     */
    private $asin;

    /**
     * @var \Genre
     *
     * @ORM\ManyToOne(targetEntity="Genre", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Code_Genre", referencedColumnName="Code_Genre")
     * })
     */
    private $codeGenre;

    /**
     * @var \Editeur
     *
     * @ORM\ManyToOne(targetEntity="Editeur", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Code_Editeur", referencedColumnName="Code_Editeur")
     * })
     */
    private $codeEditeur;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitreAlbum()
    {
        return $this->titreAlbum;
    }

    /**
     * @param string $titreAlbum
     */
    public function setTitreAlbum($titreAlbum)
    {
        $this->titreAlbum = $titreAlbum;
    }

    /**
     * @return int
     */
    public function getAnneeAlbum()
    {
        return $this->anneeAlbum;
    }

    /**
     * @param int $anneeAlbum
     */
    public function setAnneeAlbum($anneeAlbum)
    {
        $this->anneeAlbum = $anneeAlbum;
    }

    /**
     * @return binary
     */
    public function getImage()
    {
        return $this->pochette;
    }

    /**
     * @param binary $pochette
     */
    public function setPochette($pochette)
    {
        $this->pochette = $pochette;
    }

    /**
     * @return string
     */
    public function getAsin()
    {
        return $this->asin;
    }

    /**
     * @param string $asin
     */
    public function setAsin($asin)
    {
        $this->asin = $asin;
    }

    /**
     * @return Genre
     */
    public function getCodeGenre()
    {
        return $this->codeGenre;
    }

    /**
     * @param Genre $codeGenre
     */
    public function setCodeGenre($codeGenre)
    {
        $this->codeGenre = $codeGenre;
    }

    /**
     * @return Editeur
     */
    public function getCodeEditeur()
    {
        return $this->codeEditeur;
    }

    /**
     * @param Editeur $codeEditeur
     */
    public function setCodeEditeur($codeEditeur)
    {
        $this->codeEditeur = $codeEditeur;
    }




    /**
     * Get pochette
     *
     * @return binary
     */
    public function getPochette()
    {
        return $this->pochette;
    }
}
