<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Achat
 *
 * @ORM\Table(name="Achat", indexes={@ORM\Index(name="IDX_E768AB52A1866919", columns={"Code_Enregistrement"}), @ORM\Index(name="IDX_E768AB52D7B589C1", columns={"Code_Abonn�"})})
 * @ORM\Entity
 */
class Achat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Code_Achat", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Enregistrement
     *
     * @ORM\ManyToOne(targetEntity="Enregistrement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Code_Enregistrement", referencedColumnName="Code_Morceau")
     * })
     */
    private $codeEnregistrement;

    /**
     * @var \Abonn�
     *
     * @ORM\ManyToOne(targetEntity="Abonn�")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Code_Abonn�", referencedColumnName="Code_Abonn�")
     * })
     */
    private $codeAbonne;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Enregistrement
     */
    public function getCodeEnregistrement()
    {
        return $this->codeEnregistrement;
    }

    /**
     * @param Enregistrement $codeEnregistrement
     */
    public function setCodeEnregistrement($codeEnregistrement)
    {
        $this->codeEnregistrement = $codeEnregistrement;
    }

    /**
     * @return Abonn�
     */
    public function getCodeAbonne()
    {
        return $this->codeAbonne;
    }

    /**
     * @param Abonn� $codeAbonne
     */
    public function setCodeAbonne($codeAbonne)
    {
        $this->codeAbonne = $codeAbonne;
    }


}
