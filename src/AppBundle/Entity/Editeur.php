<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Editeur
 *
 * @ORM\Table(name="Editeur", indexes={@ORM\Index(name="IDX_CA1A7E7320B77BF2", columns={"Code_Pays"})})
 * @ORM\Entity
 */
class Editeur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Code_Editeur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeEditeur;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_Editeur", type="string", length=50, nullable=false)
     */
    private $nomEditeur;

    /**
     * @var \Pays
     *
     * @ORM\ManyToOne(targetEntity="Pays", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Code_Pays", referencedColumnName="Code_Pays")
     * })
     */
    private $codePays;

    /**
     * @return int
     */
    public function getCodeEditeur()
    {
        return $this->codeEditeur;
    }

    /**
     * @return string
     */
    public function getNomEditeur()
    {
        return $this->nomEditeur;
    }

    /**
     * @param string $nomEditeur
     */
    public function setNomEditeur($nomEditeur)
    {
        $this->nomEditeur = $nomEditeur;
    }

    /**
     * @return \Pays
     */
    public function getCodePays()
    {
        return $this->codePays;
    }

    /**
     * @param \Pays $codePays
     */
    public function setCodePays($codePays)
    {
        $this->codePays = $codePays;
    }


}
