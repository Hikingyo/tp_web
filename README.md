Projet 'tp_web'
=========

## Installation

### Prérequis
* PHP 5.5.12 ou plus dans le path
* le driver pdo_sqlsrv est également requis
* Avant même de cloner le dépôt, il est nécessaire d'avoir composer d'installer.
https://getcomposer.org/doc/00-intro.md

### Configuration du serveur
* Ajouter tp_web.dev dans le fichier hosts
* Ajouter le vhost dans le fichier httpdvhost.conf


```
#!apache

<VirtualHost *:80>
    ServerName tp_web.dev
    ServerAlias www.tp_web.dev

    DocumentRoot "c:/wamp/www/tp_web/web"
    <Directory "c:/wamp/www/tp_web/web">
        AllowOverride None
        Order Allow,Deny
        Allow from All
        <IfModule mod_rewrite.c>
           Options -MultiViews
           RewriteEngine On
           RewriteCond %{REQUEST_FILENAME} !-f
           RewriteRule ^(.*)$ app_dev.php [QSA,L]
       </IfModule>
   </Directory>

  # uncomment the following lines if you install assets as symlinks
  # or run into problems when compiling LESS/Sass/CoffeScript assets
  # <Directory /var/www/project>
  #     Options FollowSymlinks
  # </Directory>

  # optionally disable the RewriteEngine for the asset directories
  # which will allow apache to simply reply with a 404 when files are
  # not found instead of passing the request into the full symfony stack
   <Directory "c:/wamp/www/tp_web/web/bundles">
       <IfModule mod_rewrite.c>
           RewriteEngine Off
       </IfModule>
    </Directory>
    ErrorLog logs/project_error.log
    CustomLog logs/project_access.log combined
</VirtualHost>
```


* Redémarrer le serveur pour prendre en compte la nouvelle configuration

### Mise en place des sources
* Cloner le dépôt, de préférence dans le dossier `www` de wamp
* A la racine du projet lance l'installation des dépendances via composer :
`php composer install`, si ce dernier et dans le path

Et voilà.